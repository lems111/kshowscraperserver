module.exports = function() {
  return {
    puppeteerLaunchParams: {
      headless: true,
      args: ["--no-sandbox", "--disable-setuid-sandbox"]
    },
    pageWaitDomContentLoaded: { waitUntil: "domcontentloaded" },
    pageWaitNetworkIdle: { waitUntil: "networkidle2" }
  };
};
