const request = require("request"),
  _ = require("lodash"),
  cheerio = require("cheerio"),
  jsonframe = require("jsonframe-cheerio"),
  puppeteer = require("puppeteer"),
  config = require("../../config")(),
  streamer = require("../helpers/streamer"),
  page = require("../helpers/page");

module.exports = {
  list: function() {
    return page
      .getElementHtml(
        "http://www.dramago.com/drama-shows",
        "#body > .s_left_col",
        config.pageWaitNetworkIdle
      )
      .then(html => {
        if (html) {
          let $ = cheerio.load(html);
          let frame = {
            shows: {
              _s: ".series_index > tbody > tr > td",
              _d: [
                {
                  url: "a @ href",
                  title: "a"
                }
              ]
            }
          };
          jsonframe($);

          let result = $("#content").scrape(frame, {
            string: true
          });
          let jsonRes = JSON.parse(result);
          jsonRes.shows = _.sortBy(jsonRes.shows, ["title"]);
          return jsonRes;
        } else return null;
      })
      .catch(function(err) {
        console.log("list err:", err);
        return null;
      });
  },
  episodes: function(show) {
    return page
      .getElementHtml(
        show,
        "#body > div > #content",
        config.pageWaitNetworkIdle
      )
      .then(html => {
        if (html) {
          let $ = cheerio.load(html);
          let frame = {
            episodes: {
              _s: "li",
              _d: [
                {
                  url: "a @ href",
                  title: "a",
                  age: "span"
                }
              ]
            }
          };
          jsonframe($);

          let result = $("#videos > ul").scrape(frame, {
            string: true
          });
          return JSON.parse(result);
        } else return null;
      })
      .catch(function(err) {
        console.log("episodes err:", err);
        return null;
      });
  },
  getDownloadLinks: async function(url, oneLink) {
    let browser, page, servers, linksData, serverLinks;
    // instantiate browser
    browser = await puppeteer.launch(config.puppeteerLaunchParams);

    try {
      // instantiate tab
      page = await browser.newPage();
      page.setViewport({ width: 2000, height: 1000 });

      // go to url
      console.log("getDownloadLinks: going to " + url);
      await page.goto(url, {
        waitUntil: "networkidle2"
      });
      console.log("getDownloadLinks: arrived");

      // get stream links
      await page.waitForSelector("#streams > .vmargin > div > iframe");
      servers = await page.$$("#streams > .vmargin");
      serverLinks = await getServerLinks(page, servers);
      linksData = await getDownloadLinks(page, serverLinks, oneLink);
    } catch (err) {
      console.log("getDownloadLinks error: ", err);
    }
    // close browser
    browser.close();
    console.log(linksData);

    return linksData;
  }
};

async function getDownloadLinks(page, serverLinks) {
  var linksData = [],
    partCount,
    proceedToRealLink = false;

  try {
    // get real download links
    if (!_.isEmpty(serverLinks)) {
      for (const server of serverLinks) {
        console.log(
          "getDownloadLinks - retrieving real links for server: " + server.name
        );
        for (const linkData of server.links) {
          // get real link if this is the first time around or if this link data part number is missing from the links data array
          proceedToRealLink =
            _.isEmpty(linksData) ||
            !partCount ||
            (server.links.length == partCount &&
              !_.find(linksData, ["partNum", linkData.partNum]));

          if (proceedToRealLink && linkData && linkData.url) {
            linkData.url = await getRealLink(page, linkData);
            console.log("getDownloadLinks - linkData:", linkData);
            if (linkData.url) {
              if (!partCount) partCount = server.partCount;
              linksData.push(linkData);
            }
          } else
            console.log(
              "getDownloadLinks - don't get real link, partCount:" +
                partCount +
                ", server links count:" +
                server.links.length +
                ", link data part number: " +
                linkData.partNum
            );
        }
        if (!_.isEmpty(linksData) && partCount == linksData.length) {
          linksData = _.sortBy(linksData, ["partNum"]);
          return linksData;
        }
      }
    }
  } catch (err) {
    console.log("getDownloadLinks - url: " + linkData.url + ", error: ", err);
  }
  return null;
}

async function getServerLinks(page, servers) {
  var invalidServer,
    span,
    serverName,
    parts,
    serverLinks = [],
    partNum;

  try {
    for (const server of servers) {
      invalidServer = !!(await server.$(".error_box"));
      if (!invalidServer) {
        var links = [];
        span = await server.$("div:first-child > span");
        serverName = await page.evaluate(el => {
          return Promise.resolve(el.textContent);
        }, span);
        console.log("getServerLinks - serverName: " + serverName);
        parts = await server.$$("div:first-child > .part_list > li");
        partNum = 0;
        for (const part of parts) {
          console.log("getServerLinks - searching part");

          var linkData = await getStreamLink(page, part, serverName);
          linkData.partNum = ++partNum;
          links.push(linkData);
        }
        if (!links.includes(null))
          serverLinks.push({
            name: serverName,
            links: links,
            partCount: parts.length
          });
      }
    }
    if (!_.isEmpty(serverLinks)) {
      console.log("getServerLinks - serverLinks: ", serverLinks);
      return serverLinks;
    }
  } catch (err) {
    console.log("getServerLinks - url: " + linkData.url + ", error: ", err);
  }
  return null;
}

async function getRealLink(page, linkData) {
  let span;
  try {
    await page.goto(linkData.url, {
      waitUntil: "networkidle2"
    });
    console.log("getRealLink: arrived: ", linkData);

    // get stream links
    await page.waitForSelector("#streams > .vmargin > div > iframe");
    servers = await page.$$("#streams > .vmargin");

    for (const server of servers) {
      span = await server.$("div:first-child > span");
      serverName = await page.evaluate(el => {
        return Promise.resolve(el.textContent);
      }, span);
      if (serverName == linkData.server) {
        let iFrameplayerElement = await server.$("div > iframe"),
          iframeplayerFrame = await iFrameplayerElement.contentFrame();
        if (iframeplayerFrame) {
          let video = await iframeplayerFrame.waitForSelector(
            "#myplayer > div > video",
            { visible: true }
          );
          return await iframeplayerFrame.evaluate(el => {
            return Promise.resolve(el.src);
          }, video);
        } else console.log("failed to find iframe");
        return null;
      }
    }
  } catch (err) {
    console.log("getRealLink - url: " + linkData.url + ", error: ", err);
  }
  return null;
}

async function getStreamLink(page, link, serverName) {
  var linkData = {},
    url,
    a = await link.$("a");
  try {
    linkData.server = serverName;

    linkData.name = await page.evaluate(el => {
      return Promise.resolve(el.textContent);
    }, a);

    linkData.url = await page.evaluate(el => {
      return Promise.resolve(el.href);
    }, a);
  } catch (err) {
    console.log("getStreamLink error: ", err);
    linkData = null;
  }
  console.log("linkData: ", linkData);
  return linkData;
}
