const request = require("request"),
  _ = require("lodash"),
  cheerio = require("cheerio"),
  jsonframe = require("jsonframe-cheerio"),
  puppeteer = require("puppeteer"),
  config = require("../../config")(),
  streamer = require("../helpers/streamer"),
  page = require("../helpers/page");

module.exports = {
  list: function() {
    return page
      .getElementHtml(
        "http://kshowonline.com/show-list",
        ".container > .row > div > .content-list",
        config.pageWaitNetworkIdle
      )
      .then(html => {
        if (html) {
          let $ = cheerio.load(html);
          let frame = {
            shows: {
              _s: "li",
              _d: [
                {
                  url: "li > a @ href",
                  title: "li > a @ title"
                }
              ]
            }
          };
          jsonframe($);

          let result = $(".showlist").scrape(frame, {
            string: true
          });
          return JSON.parse(result);
        } else return null;
      })
      .catch(function(err) {
        console.log("list err:", err);
        return null;
      });
  },
  episodes: function(show) {
    return page
      .getElementHtml(
        show,
        ".container.content > .row",
        config.pageWaitNetworkIdle
      )
      .then(html => {
        if (html) {
          let $ = cheerio.load(html);
          let frame = {
            episodes: {
              _s: ".col.s12.m12.l8.content-left > .content-list.z-depth-1 > a",
              _d: [
                {
                  url: "_parent_ @ href",
                  title: "_parent_ @ title"
                }
              ]
            }
          };
          jsonframe($);

          let result = $(".col.s12.m12.l8.content-left").scrape(frame, {
            string: true
          });
          return JSON.parse(result);
        } else return null;
      })
      .catch(function(err) {
        console.log("episodes err:", err);
        return null;
      });
  },
  getEpisodeImage: async function(url) {
    let browser, page, serverList, streamLinks, image, tbody;
    // instantiate browser
    browser = await puppeteer.launch(config.puppeteerLaunchParams);
    try {
      // instantiate tab
      page = await browser.newPage();
      page.setViewport({ width: 2000, height: 1000 });

      // go to url
      console.log("getEpisodeImage: going to " + url);
      await page.goto(url, {
        waitUntil: "networkidle2"
      });
      console.log("getEpisodeImage: arrived to " + url);

      // get stream links
      await page.waitForSelector("#data-episode > table.episode");
      tbody = await page.$("table.episode > tbody");
      streamLinks = await tbody.$$("tr");

      for (const streamLink of streamLinks) {
        image = await getEpisodeImage(page, streamLink);
        if (image) break;
      }
    } catch (err) {
      console.log("getEpisodeImage error: ", err);
    }
    // close browser
    browser.close();

    return image;
  },
  getDownloadLinks: async function(url, oneLink) {
    let browser,
      page,
      tbody,
      streamLinks,
      linksData = [];
    // instantiate browser
    browser = await puppeteer.launch(config.puppeteerLaunchParams);
    try {
      // instantiate tab
      page = await browser.newPage();
      page.setViewport({
        width: 2000,
        height: 1000
      });

      // go to url
      console.log("going to " + url);
      await page.goto(url, {
        waitUntil: "networkidle2"
      });
      console.log("arrived to " + url);

      // get stream links
      await page.waitForSelector("#data-episode > table.episode");
      tbody = await page.$("table.episode > tbody");
      streamLinks = await tbody.$$("tr");

      for (const streamLink of streamLinks) {
        var linkData = {};
        linkData = await getStreamLink(page, streamLink);
        if (linkData) {
          linksData.push(linkData);
          if (oneLink === "true") break;
        }
      }
    } catch (err) {
      console.log("getDownloadLinks error: ", err);
    }
    // close browser
    browser.close();

    return linksData;
  }
};

async function getEpisodeImage(page, link) {
  var linkData = {},
    image,
    isLinkInvalid,
    a = await link.$("td > a"),
    td = await link.$("td:first-child"),
    server = await page.evaluate(el => {
      return Promise.resolve(
        el.textContent.replace("Server ", "").replace(": ", "")
      );
    }, td);
  try {
    if (!["Backup", "Backup 2", "VIP 2", "VIP HD"].includes(server)) {
      await a.click();
      await page.waitForSelector("#player_field > .watch", { visible: true });
      isLinkInvalid = !!await page.$("#player_field > .watch.text", {
        visible: true
      });
      console.log("isLinkInvalid:" + isLinkInvalid);
      if (!isLinkInvalid)
        image = await streamer.getEpisodeImage(
          page,
          server,
          "#player_field > .watch > iframe"
        );
    }
  } catch (err) {
    console.log("getEpisodeImage error: ", err);
  }
  console.log("image: ", image);
  return image;
}

async function getStreamLink(page, link) {
  let linkData = {},
    a = await link.$("td > a"),
    td = await link.$("td:first-child"),
    videoFrameData,
    isLinkInvalid;
  try {
    linkData.name = await page.evaluate(el => {
      return Promise.resolve(el.textContent);
    }, a);
    linkData.server = await page.evaluate(el => {
      return Promise.resolve(
        el.textContent.replace("Server ", "").replace(": ", "")
      );
    }, td);
    if (!["Backup", "Backup 2", "VIP 2", "VIP HD"].includes(linkData.server)) {
      await a.click();
      await page.waitForSelector("#player_field > .watch", { visible: true });
      isLinkInvalid = !!await page.$("#player_field > .watch.text", {
        visible: true
      });
      console.log("isLinkInvalid:" + isLinkInvalid);
      if (!isLinkInvalid)
        videoFrameData = await streamer.getLinkData(
          page,
          linkData.server,
          "#player_field > .watch > #player-jw",
          "#player_field > .watch > iframe"
        );

      if (!_.isEmpty(videoFrameData)) {
        _.merge(linkData, videoFrameData);
        console.log("linkData: ", linkData);
        return linkData;
      } else return null;
    } else return null;
  } catch (err) {
    console.log("getStreamLink error: ", err);
  }
}
