const request = require("request"),
  _ = require("lodash"),
  cheerio = require("cheerio"),
  jsonframe = require("jsonframe-cheerio"),
  puppeteer = require("puppeteer"),
  config = require("../../config")(),
  streamer = require("../helpers/streamer"),
  page = require("../helpers/page");

module.exports = {
  list: function() {
    return page
      .getElementHtml(
        "http://kshow123.net/show/",
        "#main",
        config.pageWaitDomContentLoaded
      )
      .then(html => {
        if (html) {
          let $ = cheerio.load(html);
          let frame = {
            shows: {
              _s: "li",
              _d: [
                {
                  url: "li > a @ href",
                  title: "li > a"
                }
              ]
            }
          };
          jsonframe($);

          let result = $("#featured").scrape(frame, {
            string: true
          });
          return JSON.parse(result);
        } else return null;
      })
      .catch(function(err) {
        console.log("list err:", err);
        return null;
      });
  },
  episodes: function(show) {
    return page
      .getElementHtml(show, "#list-episodes", config.pageWaitDomContentLoaded)
      .then(html => {
        if (html) {
          let $ = cheerio.load(html);
          let frame = {
            episodes: {
              _s: "tr",
              _d: [
                {
                  url: "td > h2 > a @ href",
                  title: "td > h2 > a @ title",
                  age: "td:last-child"
                }
              ]
            }
          };
          jsonframe($);

          let result = $("tbody.list-episode").scrape(frame, {
            string: true
          });
          return JSON.parse(result);
        } else return null;
      })
      .catch(function(err) {
        console.log("episodes err:", err);
        return null;
      });
  },
  getEpisodeImage: async function(url) {
    let browser, page, serverList, streamLinks, image;
    // instantiate browser
    browser = await puppeteer.launch(config.puppeteerLaunchParams);
    try {
      // instantiate tab
      page = await browser.newPage();
      page.setViewport({ width: 2000, height: 1000 });

      // go to url
      console.log("getEpisodeImage: going to " + url);
      await page.goto(url, {
        waitUntil: "networkidle2"
      });
      console.log("getEpisodeImage: arrived to " + url);

      // get stream links
      serverList = await page.waitForSelector("#server_list", {
        visible: true
      });

      streamLinks = await serverList.$$("#server_list > .server_item");
      for (const streamLink of streamLinks) {
        image = await getEpisodeImage(page, streamLink);
        if (image) break;
      }
    } catch (err) {
      console.log("getEpisodeImage error: ", err);
    }
    // close browser
    browser.close();

    return image;
  },
  getDownloadLinks: async function(url, oneLink) {
    let browser,
      page,
      serverList,
      streamLinks,
      linksData = [],
      videoArrayString,
      videoArray;
    // instantiate browser
    browser = await puppeteer.launch(config.puppeteerLaunchParams);
    try {
      // instantiate tab
      page = await browser.newPage();
      page.setViewport({ width: 2000, height: 1000 });

      // go to url
      console.log("getDownloadLinks: going to " + url);
      await page.goto(url, {
        waitUntil: "networkidle2"
      });
      console.log("getDownloadLinks: arrived to " + url);

      // get stream links
      serverList = await page.waitForSelector("#server_list", {
        visible: true
      });

      streamLinks = await serverList.$$("#server_list > .server_item");
      for (const streamLink of streamLinks) {
        var linkData = await getStreamLink(page, streamLink);
        if (linkData) {
          linksData.push(linkData);
          if (oneLink === "true") break;
        }
      }

      console.log(linksData);
    } catch (err) {
      console.log("getDownloadLinks error: ", err);
    }
    // close browser
    browser.close();
    return linksData;
  }
};

async function getEpisodeImage(page, link) {
  var linkData = {},
    image,
    a = await link.$(".video_list > li > a"),
    strong = await link.$(".server_item > strong"),
    server = await page.evaluate(el => {
      return Promise.resolve(el.textContent.replace(": ", ""));
    }, strong);
  try {
    if (!["V.I.P 2", "Daily"].includes(server)) {
      await a.click();
      await page.waitForSelector("#player > div > #mediaplayer", {
        visible: true
      });
      image = await streamer.getEpisodeImage(
        page,
        server,
        "#player > div > #mediaplayer > iframe"
      );
    }
  } catch (err) {
    console.log("getEpisodeImage error: ", err);
  }
  console.log("image: ", image);
  return image;
}

async function getStreamLink(page, link) {
  var linkData = {},
    videoFrameData,
    a = await link.$(".video_list > li > a"),
    strong = await link.$(".server_item > strong");
  try {
    linkData.name = await page.evaluate(el => {
      return Promise.resolve(el.textContent);
    }, a);
    linkData.server = await page.evaluate(el => {
      return Promise.resolve(el.textContent.replace(": ", ""));
    }, strong);
    if (!["V.I.P 2", "Daily"].includes(linkData.server)) {
      await a.click();
      await page.waitForSelector("#player > div > #mediaplayer", {
        visible: true
      });
      videoFrameData = await streamer.getLinkData(
        page,
        linkData.server,
        "#player > div > #mediaplayer.jwplayer",
        "#player > div > #mediaplayer > iframe"
      );

      if (!_.isEmpty(videoFrameData)) {
        _.merge(linkData, videoFrameData);
        console.log("linkData: ", linkData);
        return linkData;
      } else return null;
    } else return null;
  } catch (err) {
    console.log("getStreamLink error: ", err);
  }
  console.log("linkData: ", linkData);
  return linkData;
}
