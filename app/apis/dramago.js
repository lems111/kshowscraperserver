const express = require("express"),
  router = express.Router(),
  _ = require("lodash"),
  scraper = require("../scrapers/dramago");

router.get("/shows", function(req, res) {
  scraper
    .list()
    .then(function(list) {
      res.json(list);
    })
    .catch(function(err) {
      console.log("shows err:", err);
      res.json(null);
    });
});

router.get("/episodes", function(req, res) {
  console.log("show: ", req.query.show);
  if (req.query.show != 'undefined') {
    scraper
      .episodes(req.query.show)
      .then(function(list) {
        res.json(list);
      })
      .catch(function(err) {
        console.log("episodes err:", err);
        res.json(null);
      });
  } else res.json(null);
});

router.get("/links", function(req, res) {
  console.log("links: ", req.query);
  scraper
    .getDownloadLinks(req.query.episode, req.query.oneLink)
    .then(links => {
      returnArray = [];
      if (links) {
        for (const link of links) {
          if (!_.isEmpty(link) && link.server && link.url && link.name) {
            let server = _.find(returnArray, ["server", link.server]);

            if (server) server.links.push({ name: link.name, url: link.url });
            else {
              server = {
                server: link.server,
                links: [{ name: link.name, url: link.url }]
              };
              returnArray.push(server);
            }
          }
        }
      }
      console.log("returnArray: ", returnArray);
      res.json({ links: returnArray });
    })
    .catch(err => {
      console.log("links err:", err);
      res.json(null);
    });
});

module.exports = router;
