const _ = require("lodash"),
  puppeteer = require("puppeteer"),
  config = require("../../config")();

module.exports = {
  getLinkData: async function(page, server, jwPlayerSelector, iframeSelector) {
    try {
      switch (server.toLowerCase()) {
        case "google":
          return GetLinkDataFromJwPlayer(page, server, jwPlayerSelector);
        case "openload":
        case "streamango":
        case "video+":
        case "v.i.p":
        case "rapidvideo":
          return await GetLinkDataFromIframe(page, server, iframeSelector);
        default:
          console.log("getLinkData unknown server : ", server);
      }
    } catch (err) {
      console.log("getLinkData error: ", err);
    }
    return null;
  },
  getEpisodeImage: async function(page, server, iframeSelector) {
    let image;
    try {
      switch (server.toLowerCase()) {
        case "openload":
        case "streamango":
        case "video+":
        case "v.i.p":
        case "rapidvideo":
          console.log("starting to get episode image for " + server);
          let iFrameplayerElement = await page.waitForSelector(iframeSelector, {
              visible: true
            }),
            iframeplayerFrame = await iFrameplayerElement.contentFrame();

          // wait for iframe to fully load
          await iframeplayerFrame.waitForSelector("body", { visible: true });
          if (await IsIframeValid(iframeplayerFrame, server))
            image = await getEpisodeImage(iframeplayerFrame, server);
          else console.log(server + " - image is no longer valid");

          await iFrameplayerElement.dispose();

          console.log("getEpisodeImage: image ", image);
        default:
          console.log("getLinkData unknown server : ", server);
      }
    } catch (err) {
      console.log("getLinkData error: ", err);
    }
    return image;
  }
};

async function GetLinkDataFromJwPlayer(page, server, jwPlayerSelector) {
  try {
    console.log("starting to get linkdata for " + server + " is jwPlayer");
    let videoElement = await page.waitForSelector(
        jwPlayerSelector + " > .jw-media > video.jw-video",
        {
          visible: true
        }
      ),
      url = await page.evaluate(el => {
        return Promise.resolve(el.src);
      }, videoElement);
    console.log("url: " + url);
    return { url: url };
  } catch (err) {
    console.log("GetLinkDataFromJwPlayer error: ", err);
  }
  return null;
}

async function GetLinkDataFromIframe(page, server, iframeSelector) {
  try {
    console.log("starting to get linkdata for " + server + " is iFrame");
    let iFrameplayerElement = await page.waitForSelector(iframeSelector, {
        visible: true
      }),
      iframeplayerFrame = await iFrameplayerElement.contentFrame(),
      iframeData;
    // wait for iframe to fully load
    await iframeplayerFrame.waitForSelector("body", { visible: true });
    if (await IsIframeValid(iframeplayerFrame, server))
      iframeData = await GetIframeData(iframeplayerFrame, server);
    else console.log(server + " - link is no longer valid");

    await iFrameplayerElement.dispose();

    console.log("GetLinkDataFromIframe: iframeData ", iframeData);
    return iframeData;
  } catch (err) {
    console.log("GetLinkDataFromIframe error: ", err);
  }
  return null;
}

async function getEpisodeImage(iframeplayerFrame, server) {
  let urlElement,
    poster = null;
  console.log("getEpisodeImage: starting...");
  try {
    switch (server.toLowerCase()) {
      case "openload":
        let videooverlay = await iframeplayerFrame.$("#videooverlay");
        await videooverlay.click();
        urlElement = await iframeplayerFrame.waitForSelector(
          "#olvideo_html5_api[src]"
        );
        poster = await iframeplayerFrame.evaluate(el => {
          return Promise.resolve(el.poster);
        }, urlElement);
        return poster;
      case "streamango":
      case "video+":
      case "v.i.p":
        urlElement = await iframeplayerFrame.waitForSelector(
          "#mgvideo_html5_api"
        );
        poster = await iframeplayerFrame.evaluate(el => {
          return Promise.resolve(el.poster);
        }, urlElement);
        return poster;
      case "rapidvideo":
        urlElement = await iframeplayerFrame.waitForSelector(
          "#videojs_html5_api"
        );
        poster = await iframeplayerFrame.evaluate(el => {
          return Promise.resolve(el.poster);
        }, urlElement);
        return poster;
      default:
        return poster;
    }
  } catch (err) {
    console.log("getEpisodeImage error: ", err);
  }
  return null;
}

async function GetIframeData(iframeplayerFrame, server) {
  let urlElement,
    iframeData = {};
  console.log("GetIframeData: starting...");
  try {
    if (!iframeplayerFrame.isDetached()) {
      switch (server.toLowerCase()) {
        case "openload":
          let videooverlay = await iframeplayerFrame.$("#videooverlay");
          await videooverlay.click();
          urlElement = await iframeplayerFrame.waitForSelector(
            "#olvideo_html5_api[src]"
          );
          iframeData.url = await iframeplayerFrame.evaluate(el => {
            return Promise.resolve(el.src);
          }, urlElement);
          return iframeData;
        case "streamango":
        case "video+":
        case "v.i.p":
          urlElement = await iframeplayerFrame.waitForSelector(
            "#mgvideo_html5_api"
          );
          iframeData.url = await iframeplayerFrame.evaluate(el => {
            return Promise.resolve(el.src);
          }, urlElement);
          return iframeData;
        case "rapidvideo":
          urlElement = await iframeplayerFrame.waitForSelector(
            "#videojs_html5_api"
          );
          iframeData.url = await iframeplayerFrame.evaluate(el => {
            return Promise.resolve(el.src);
          }, urlElement);
          return iframeData;
        default:
          return iframeData;
      }
    }
  } catch (err) {
    console.log("GetIframeData error: ", err);
  }
  return null;
}

async function IsIframeValid(iframeplayerFrame, server) {
  let errElement;
  try {
    console.log("found " + server.toLowerCase() + " - checking if link exists");
    switch (server.toLowerCase()) {
      case "openload":
        errElement = !!await iframeplayerFrame.$(".content-blocked");
        if (errElement) return false;
        break;
      case "streamango":
      case "video+":
      case "v.i.p":
        errElement = !!await iframeplayerFrame.$("#page-404");
        console.log("errElement: " + errElement);
        if (errElement) return false;
        else {
          let errMsg;
          errElement = !!await iframeplayerFrame.$(
            "div.site-index > .jumbotron > h1"
          );
          if (errElement)
            errMsg = await iframeplayerFrame.$eval(
              "div.site-index > .jumbotron > h1",
              el => el.textContent
            );
          if (errMsg === "Sorry!") return false;
        }
        break;
      case "rapidvideo":
        return true;
        break;
      default:
        return false;
    }
  } catch (err) {
    console.log("GetIframeData error: ", err);
  }
  return true;
}
