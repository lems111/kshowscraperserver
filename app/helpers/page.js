const _ = require("lodash"),
  puppeteer = require("puppeteer"),
  config = require("../../config")();

module.exports = {
  getElementHtml: async function(url, selector, pageOptions) {
    let browser, page, elHandle, html;
    browser = await puppeteer.launch(config.puppeteerLaunchParams);
    try {
      // instantiate browser

      // instantiate tab
      page = await browser.newPage();
      page.setViewport({
        width: 2000,
        height: 1000
      });

      // go to url
      console.log("going to " + url);
      await page.goto(url, pageOptions);
      console.log("arrived to " + url);

      elHandle = await page.$(selector);
      html = await page.evaluate(el => el.innerHTML, elHandle);
      await elHandle.dispose();
      browser.close();
    } catch (err) {
      browser.close();
      console.log("getElementHtml error: ", err);
    }
    return html;
  }
};
