// modules
const express = require("express"),
  app = express(),
  router = express.Router(),
  bodyParser = require("body-parser"),
  config = require("./config"),
  server = require("http").createServer(app),
  port = process.env.PORT || 3000,
  _ = require("lodash");

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(bodyParser.json());
app.use(
  bodyParser.json({
    type: "application/vnd.api+json"
  })
);
app.use(express.static(__dirname + "/app/public"));
app.use("/apis/kshow-online", require(__dirname + "/app/apis/kshowOnline"));
app.use("/apis/kshow123", require(__dirname + "/app/apis/kshow123"));
app.use("/apis/dramago", require(__dirname + "/app/apis/dramago"));
app.get("*", function(req, res) {
  res.sendFile(__dirname + "/app/public/index.html"); // load our public/index.html file
});
server.listen(port);
