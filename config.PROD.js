module.exports = function() {
  return {
    puppeteerLaunchParams: {
      headless: true,
      executablePath: "/usr/bin/chromium-browser"
    },
    pageWaitDomContentLoaded: { waitUntil: "domcontentloaded" },
    pageWaitNetworkIdle: { waitUntil: "networkidle2" }
  };
};
